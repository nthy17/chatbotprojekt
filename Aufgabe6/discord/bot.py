import hikari
import requests
import json
# Discord Bot authorisieren mit Token
bot = hikari.GatewayBot(token='OTMxODUxMzAzMTEyNTQ0MjU2.YeKcJw.B7TgEUgqLpqOSLsw-xYJ_u6TtS0')

# Bot wartet auf Nachrichten
@bot.listen(hikari.GuildMessageCreateEvent)
async def ping(event):
    # Wenn Nachricht vom Bot selbst ist, weiter warten
    if event.is_bot or not event.content:
        return

    # Wenn Nachricht ein "Ping" ist, sende "Pong" zurück
    if event.content.startswith("ys.ping"):
        await event.message.respond("Pong!")

    # Wenn nachricht mit ys. beginnt und kein Pong ist, sende alles hinter dem ys. als Nachricht zum Bot
    if event.content.startswith("ys."):
        message = event.content.lstrip("ys.")
        data = json.dumps({"sender": "test_user", "message": f"{message}"})
        response = requests.post('http://localhost:5005/webhooks/rest/webhook', data=data)
        responseJson = response.json()
        for message in responseJson:
            await event.message.respond(message["text"])

# Bot starten
bot.run()
