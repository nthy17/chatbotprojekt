# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions

import json  # Json Bibliothek (einfache Funktionen zur bearbeitung von Json Results)
import random  # Random Library
from typing import Any, Text, Dict, List  # Import des "Dict" Types, der für Rasa benötigt wird
from googleapiclient.discovery import build  # Import des Youtube API Managers
from rasa_sdk import Action, Tracker  # Action und Tracker von Rasa
from rasa_sdk.events import SlotSet  # SlotSet Event von Rasa
from rasa_sdk.executor import CollectingDispatcher  # Import des Dispatchers

API_KEY = "AIzaSyCnuM8XA6jdXiHRe423oGe73HueRzsNBXE"  # API Schlüssel der Youtube API
MAX_DATA = 49  # Maximale Anzahl an Datensätzen im Result
MIN_DATA = 5  # Minimale Anzahl an Datensätzen im Result
categoryID = {  # Datensatz Thema -> "CategoryID", welcher für einige API-Abfragen benötigt wird
    'gaming': "20",
    'Gaming': "20",
    'music': "10",
    'Music': "10",
    'Animals': "15",
    'animals': "15",
    'sports': "17",
    'Sports': "17",
    'comedy': "23",
    'Comedy': "23",
    'entertainment': "24",
    'Entertainment': "24",
    'animation': "1",
    'Animation': "1",
    'cars': "2",
    'Cars': "2",
    'vehicles': "2",
    'Vehicles': "2",
    'shorts': "18",
    'Shorts': "18",
    'vlogs': "21",
    'Vlogs': "21",
    'news': "25",
    'News': "25"
}


class ActionGetRandomVideo(Action):
    # Diese Custom Action sucht mit der Youtube API nach einem zufälligen Video.
    # Wird ein Thema in der Nachricht als Entity erkannt, wird dieses Thema als Filter für die Suche genutzt
    # Zugelassene Themen (von Youtube) zur eingrenzung: gaming, news, sports, animals

    def name(self) -> Text:  # Weist der Custom Action den Namen "action_get_random_video" zu
        return "action_get_random_video"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any], ) -> List[
        Dict[Text, Any]]:

        # Baut Verbindung zur Youtube API Request Pipeline auf
        youtube = build('youtube', 'v3', developerKey=API_KEY)

        # Speichert das "topic"(Entity), welches in der letzten Nachricht erwähnt wurde in einer Variable
        currentTopic = next(tracker.get_latest_entity_values("topic"), None)

        if not currentTopic:  # Wenn kein "topic" gefunden wurde wird ein zufälliges, trendendes Video zurückgegeben

            # Request an die Youtube API mit folgenden Parametern:
            # part: Was soll zurückgegeben werden?; chart: Auf welcher Liste wird gesucht?
            # hl: Welche Sprache soll das Video bevorzugt haben?; maxResults: Wie viele Ergebnisse?
            # regionCode: In welcher Region soll das Video am trenden sein?

            request = youtube.videos().list(
                part='contentDetails',
                chart='mostPopular',
                hl='en',
                maxResults=MAX_DATA,
                regionCode='US'
            )
            response = request.execute()  # Request ausführen

            # Video Link und Tipp zur Suche zurück geben
            dispatcher.utter_message("Here is a video for you! You can also give me a category like gaming, news,"
                                     "... , to get a random video of that category!")
            dispatcher.utter_message(
                "https://www.youtube.com/watch?v=" + response["items"][random.randint(0, MAX_DATA)]["id"])
            return []

        # Wenn ein Topic in der Nachricht gefunden wurde, prüfen, ob es von Youtube unterstützt wird
        category = categoryID.get(currentTopic, None)
        if not category:  # Wenn das Topic nicht im Datensatz vorhanden ist, nach Rechtschreibung fragen
            dispatcher.utter_message(f"I did not recognize {currentTopic}. Is it spelled correctly?")
            return []
        # Youtube API Request, dieses Mal aber mit VideoCategory
        request = youtube.videos().list(
            part='contentDetails',
            chart='mostPopular',
            hl='en',
            maxResults=MAX_DATA,
            regionCode='US',
            videoCategoryId=category
        )
        response = request.execute()  # Request ausführen

        # Video Link und entsprechendes Topic zurückgeben
        dispatcher.utter_message(f"Here is a video for you! Its from {currentTopic}, since that's what you like!")
        dispatcher.utter_message(
            "https://www.youtube.com/watch?v=" + response["items"][random.randint(0, MAX_DATA-1)]["id"])
        return []


class ActionGetVideoOfYoutuber(Action):
    # Diese Action gibt das neueste Video eines gegebenen Youtubers zurück
    # Wenn kein Youtuber erkannt wurde, wird die Action gestoppt und der Nutzer informiert
    # Im Fall, dass der Youtuber in letzter Zeit keine Videos hochgeladen hat, wird dies auch dem Nutzer mitgeteilt

    def name(self) -> Text:  # Weist der Custom Action den Namen "action_get_video_of_youtuber" zu
        return "action_get_video_of_youtuber"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any], ) -> List[
        Dict[Text, Any]]:

        # Baut Verbindung zur Youtube API Request Pipeline auf
        youtube = build('youtube', 'v3', developerKey=API_KEY)

        # Speichert das "ORG"(Entity), welche in der letzten Nachricht erwähnt wurde in einer Variable
        currentYoutuber = next(tracker.get_latest_entity_values("ORG"), None)

        if not currentYoutuber:  # Wenn kein Youtuber erkannt wurde
            dispatcher.utter_message(
                "Sorry i did not recognize a youtuber there... Did you perhaps spell it wrong? (Try lowercase or dont use a whitespace (-: !)")
            return []

        # Mithilfe des erkannten Youtubers den Kanal herausfinden
        request1 = youtube.search().list(
            part='snippet',
            maxResults=MIN_DATA,
            q=f"{currentYoutuber}",
        )
        response1 = request1.execute()
        for item in response1["items"]:
            if item["id"]["kind"] == "youtube#channel":
                # Im erkannten Kanal nach den neuesten Aktivitäten suchen
                request2 = youtube.activities().list(
                    part='contentDetails',
                    channelId=f"{item['id']['channelId']}",
                    maxResults=MIN_DATA
                )
                response2 = request2.execute()
                dispatcher.utter_message("Uhm, " f"{item['snippet']['title']}? " "i Think i got something for that one:")
                for res in response2["items"]:
                    # Wenn ein Video unter den neuesten Aktivitäten ist, dieses ausgeben
                    if len(res["contentDetails"]) > 0:
                        dispatcher.utter_message(
                            "A recent video -> " f"https://www.youtube.com/watch?v={res['contentDetails']['upload']['videoId']}")
                        return []
                    # Wenn kein Video gefunden wurde den Nutzer darauf hinweisen
                dispatcher.utter_message("Uhh my bad, this channels activities been mostly not related to any videos!")
                return []
            return []
        return []


class ActionGetPlaylistOfYoutuber(Action):
    # Diese Action gibt dem Nutzer die neueste Playlist eines gegebenen Nutzers zurück
    # Wenn kein Youtuber erkannt wird, wird der Nutzer darauf hingewiesen

    def name(self) -> Text: # Weist der Custom Action den Namen "action_get_playlist_of_youtuber" zu
        return "action_get_playlist_of_youtuber"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any], ) -> List[
        Dict[Text, Any]]:

        # Baut Verbindung zur Youtube API Request Pipeline auf
        youtube = build('youtube', 'v3', developerKey=API_KEY)

        # Speichert die "ORG"(Entity), welche in der letzten Nachricht erwähnt wurde in einer Variable
        currentYoutuber = next(tracker.get_latest_entity_values("ORG"), None)

        if not currentYoutuber:  # Wenn kein Youtuber erkannt wurde
            dispatcher.utter_message(
                "Sorry, please tell me a youtuber, that I can take a playlist from!")
            return []

        # Mit dem gegebenen Youtuber den Kanal finden
        request1 = youtube.search().list(
            part='snippet',
            maxResults=MIN_DATA,
            q=f"{currentYoutuber}",
        )
        response1 = request1.execute()
        for item in response1["items"]:
            # Wenn ein Channel gefunden wurde, dann die neueste Playlist zurückgeben
            if item["id"]["kind"] == "youtube#channel":
                request2 = youtube.playlists().list(
                    part='contentDetails',
                    channelId=f"{item['id']['channelId']}",
                    maxResults=MIN_DATA
                )
                response2 = request2.execute()
                for res in response2["items"]:
                    dispatcher.utter_message(
                        "Uhm, " f"{item['snippet']['title']}? " "i Think i got a playlist for that one:")
                    dispatcher.utter_message(
                        "A recent playlist -> " f"https://www.youtube.com/playlist?list={res['id']}")
                    return []
            return []
        return []


class ActionGetThumbnailOfVideo(Action):
    # Diese Action gibt dem Nutzer das Thumbnail zu einem Video zurück.
    # Das Video kann entweder nach Titel oder per Link gesucht werden
    # Link ist genauer, da es viele Videos mit dem gleichen Titel geben kann

    def name(self) -> Text: # Weist der Custom Action den Namen "action_get_thumbnail_of_video" zu
        return "action_get_thumbnail_of_video"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any], ) -> List[
        Dict[Text, Any]]:

        # Baut Verbindung zur Youtube API Request Pipeline auf
        youtube = build('youtube', 'v3', developerKey=API_KEY)

        # Speichert den "title"(Entity), welcher in der letzten Nachricht erwähnt wurde in einer Variable
        currentVideo = next(tracker.get_latest_entity_values("title"), None)

        if not currentVideo:  # Wenn kein Titel erkannt wurde
            dispatcher.utter_message(
                "Sorry i did not recognize a video title there... Did you perhaps spell it wrong? (Try lowercase or dont use a whitespace (-: !)")
            return []
        # Mit dem gegebenen Link oder Titel nach Video suchen
        request = youtube.search().list(
            part='snippet',
            maxResults=MIN_DATA,
            q=f"{currentVideo}",
            regionCode='US'
        )
        response = request.execute()

        # Thumbnail als Link zurückgeben
        for item in response["items"]:
            if item["id"]["kind"] == "youtube#video":
                thumbnailLink = item["snippet"]["thumbnails"]["high"]["url"]
                dispatcher.utter_message("Here is the thumbnail to your search:")
                dispatcher.utter_message(f"{thumbnailLink}")
            return []
        return []


class ActionUtterProposeYoutuber(Action):
    # Diese Action schlägt dem Nutzer einen Youtuber zu seinem lieblings Thema vor
    # Wenn der Nutzer zuvor kein lieblings Thema angegeben hat, wir ihm die "Trending Page" empfohlen

    def name(self) -> Text:     # Weist der Custom Action den Namen "action_utter_propose_youtuber" zu
        return "action_utter_propose_youtuber"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any], ) -> List[
        Dict[Text, Any]]:

        # Den derzeitigen Wert vom Slot "topic" in eine Variable speichern
        currentTopic = tracker.get_slot("topic")

        # Dict mit verschiedenen Youtuber-Empfehlungen zu Themen
        options = {
            "gaming": ["PewDiePie", "Markiplier", "Ninja", "PopularMMOs", "Jelly", "SSundee", "The Game Theorist"],
            "Gaming": ["PewDiePie", "Markiplier", "Ninja", "PopularMMOs", "Jelly", "SSundee", "The Game Theorist"],
            "vlogs": ["Casey Neistat", "David Dobrik", "Rachel Aust", "Ali Abdaal", "Nayna Florence", "Roman Atwood Vlogs", "Fun For Louis"],
            "Vlogs": ["Casey Neistat", "David Dobrik", "Rachel Aust", "Ali Abdaal", "Nayna Florence", "Roman Atwood Vlogs", "Fun For Louis"],
            "food": ["Joshua Weissman", "Nick DiGiovanni", "Rosanna Pansino", "Binging With Babish", "You Suck At Cooking", "Hellthy Junkfood", "Pro Home Cooks"],
            "Food": ["Joshua Weissman", "Nick DiGiovanni", "Rosanna Pansino", "Binging With Babish", "You Suck At Cooking", "Hellthy Junkfood", "Pro Home Cooks"],
            "comedy": ["Josh Wolf", "Max Adlersson", "Inkognito Spastiko", "Rebekah Wing", "MoniLina", "Tuvok12", "Daniel LaBelle"],
            "Comedy": ["Josh Wolf", "Max Adlersson", "Inkognito Spastiko", "Rebekah Wing", "MoniLina", "Tuvok12", "Daniel LaBelle"],
            "entertainment": ["EvanEraTV", "Explosm Entertainment", "Dude Perfect", "Screen Rant", "Buzz Feed", "Joey Graceffa", "Zach King"],
            "Entertainment": ["EvanEraTV", "Explosm Entertainment", "Dude Perfect", "Screen Rant", "Buzz Feed", "Joey Graceffa", "Zach King"],
            "music": ["AndrewHuang", "Rick Beato", "Rob Scallon", "RoomieOfficial", "Music is Win", "Jared Dines", "Marti Fischer"],
            "Music": ["AndrewHuang", "Rick Beato", "Rob Scallon", "RoomieOfficial", "Music is Win", "Jared Dines", "Marti Fischer"],
            "makeup": ["JamesCharles", "Jeffreestar", "Jacelyn Hill", "Manny Mua", "Laura Le", "Tati", "KathleenLights"],
            "Makeup": ["JamesCharles", "Jeffreestar", "Jacelyn Hill", "Manny Mua", "Laura Le", "Tati", "KathleenLights"],
            "fashion": ["Justin", "Tess Christine", "Vanessa Ziletti", "Jenn Im", "Samantha Maria", "Dre Drexler", "Audrey Coyne"],
            "Fashion": ["Justin", "Tess Christine", "Vanessa Ziletti", "Jenn Im", "Samantha Maria", "Dre Drexler", "Audrey Coyne"]
        }

        if not currentTopic:    # Wenn kein topic gefunden
            dispatcher.utter_message("Since you don't have a favourite topic, go on the trending page, there will "
                                     "surely be something for you as well!")
            return []

        # Empfehlung zum gefundenen topic zurückgeben
        dispatcher.utter_message(f"That's okay, since you like {currentTopic}, maybe try {random.choice(options.get(currentTopic))}!")
        return []


class ActionGetVideo(Action):
    # Diese Action gibt dem Nutzer das relevanteste Video zu einem Suchbegriff zurück
    # Sollte kein Videotitel erkannt werden, wird der Nutzer darauf hingewiesen

    def name(self) -> Text:  # Weist der Custom Action den Namen "action_get_video" zu
        return "action_get_video"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any], ) -> List[
        Dict[Text, Any]]:

        # Baut Verbindung zur Youtube API Request Pipeline auf
        youtube = build('youtube', 'v3', developerKey=API_KEY)

        # Speichert den Suchbegriff in einer Variable
        currentVideo = tracker.latest_message["text"]
        currentVideo = currentVideo.split('"')

        if not currentVideo[1]:  # Wenn kein Titel erkannt wurde
            dispatcher.utter_message(
                'Sorry i did not recognize a videotitle there... Please put it in quotation marks! (search for "title")')
            return []

        # Youtube API Request mit dem erkannten Titel als "query(q=f"{currentVideo[1]}")"
        request = youtube.search().list(
            part='snippet',
            maxResults=MIN_DATA,
            q=f"{currentVideo[1]}",
            regionCode='US'
        )
        response = request.execute()  # Request ausführen

        # Erstes gefundenes Video zurückgeben
        for item in response["items"]:
            if item["id"]["kind"] == "youtube#video":
                videoId = item["id"]["videoId"]
                dispatcher.utter_message("Here is a video to your search!:")
                dispatcher.utter_message(f"https://www.youtube.com/watch?v={videoId}")
            return []
        return []
