import spacy
# Visualizer for Spacy outputs
from spacy import displacy

nlp = spacy.load("en_core_web_trf")
doc = nlp("Apple is looking at buying U.K. startup for $1 billion")
print("\n")
print("Linguistic annotation:")
print("\n")
for token in doc:
    print(token.text, token.pos_, token.dep_)


print("\n")
print("Tags and Dependencies:")
print("\n")
for token in doc:
    print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
            token.shape_, token.is_alpha, token.is_stop)

# Displays the text dependencies in a Browser
displacy.serve(doc, style="dep")