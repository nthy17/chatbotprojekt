import spacy
from spacy import displacy

# Test Programm um Spacy's Entity Extraction zu prüfen

# Vortrainiertes Modell laden
nlp = spacy.load("en_core_web_trf")

# Natural language processing 2 Texte prüfen lassen
doc = nlp("Apple is looking at buying U.K. startup for $1 billion.")
doc2 = nlp("Our professor Frank Kammer told us something about Google.")

# Text 1 mit seinen jeweiligen erkannten Entities ausgeben lassen
print("\n")
print("Entities 1: Apple is looking at buying U.K. startup for $1 billion.")
print("\n")
for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)

# Text 2 mit seinen jeweiligen erkannten Entities asugeben lassen
print("\n")
print("Entities 2: Our professor Frank Kammer told us something about Google.")
print("\n")
for ent in doc2.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)
displacy.serve([doc, doc2], style="ent")


